CREATE TABLE users (
                       user_id INTEGER NOT NULL,
                       user_type VARCHAR(255) NOT NULL,
                       first_name VARCHAR(255),
                       last_name VARCHAR(255),
                       email VARCHAR(255),
                       is_active BOOLEAN,
                       PRIMARY KEY (user_id, user_type)
);

CREATE TABLE orders (
                       id INTEGER NOT NULL,
                       type VARCHAR(255) NOT NULL,
                       weight INTEGER,
                       sum INTEGER,
                       PRIMARY KEY (id, type)
);

INSERT INTO users (user_id, user_type, first_name, last_name, email, is_active)
VALUES (1, 'admin', 'John', 'Doe', 'john.doe@example.com', TRUE);

INSERT INTO users (user_id, user_type, first_name, last_name, email, is_active)
VALUES (2, 'user', 'Jane', 'Smith', 'jane.smith@example.com', TRUE);

INSERT INTO users (user_id, user_type, first_name, last_name, email, is_active)
VALUES (3, 'guest', 'Alice', 'Johnson', 'alice.johnson@example.com', FALSE);

INSERT INTO users (user_id, user_type, first_name, last_name, email, is_active)
VALUES (4, 'user', 'Bob', 'Brown', 'bob.brown@example.com', TRUE);

INSERT INTO users (user_id, user_type, first_name, last_name, email, is_active)
VALUES (5, 'admin', 'Carol', 'Davis', 'carol.davis@example.com', TRUE);

INSERT INTO orders (id, type, weight, sum)
VALUES (1, 'simple', '10', '100');

INSERT INTO orders (id, type, weight, sum)
VALUES (2, 'bundle', '20', '250');

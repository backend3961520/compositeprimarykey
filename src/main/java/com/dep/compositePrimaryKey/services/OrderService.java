package com.dep.compositePrimaryKey.services;
import java.util.Optional;
import com.dep.compositePrimaryKey.entities.Order;
import com.dep.compositePrimaryKey.repositories.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderService {

	private final OrderRepository orderRepository;

	@Autowired
	public OrderService(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	public Optional<Order> findOrder(int orderId, String orderType) {
		try {
			return orderRepository.findOrder(orderId, orderType);
		} catch (Exception e) {
			log.error("Error while finding order with orderId: {} and orderType: {}. Error message: {}", orderId, orderType, e.getMessage());
		}
		return Optional.empty();
	}
}

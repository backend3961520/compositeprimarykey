package com.dep.compositePrimaryKey.repositories;
import java.util.Optional;
import com.dep.compositePrimaryKey.entities.Order;
import com.dep.compositePrimaryKey.entities.OrderPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, OrderPK> {
	@Query("SELECT o FROM Order o WHERE o.orderPK.id = :orderId AND o.orderPK.type = :orderType")
	Optional<Order> findOrder(int orderId, String orderType);
}

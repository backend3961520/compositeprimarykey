package com.dep.compositePrimaryKey.entities;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "orders")
@Data
public class Order {
	@EmbeddedId
	private OrderPK orderPK;

	private int weight;
	private int sum;
}

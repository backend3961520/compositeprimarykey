package com.dep.compositePrimaryKey.entities;
import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@NoArgsConstructor
@Getter
public class OrderPK implements Serializable {
	private int id;
	private String type;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof OrderPK orderPK)) return false;
        return id == orderPK.id && Objects.equals(type, orderPK.type);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, type);
	}
}

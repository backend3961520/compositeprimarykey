package com.dep.compositePrimaryKey.controllers;
import java.util.Optional;
import com.dep.compositePrimaryKey.entities.Order;
import com.dep.compositePrimaryKey.services.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
@Slf4j
public class OrderController {

	private final OrderService orderService;

	@Autowired
	public OrderController(OrderService orderService) {
		this.orderService = orderService;
	}

	@GetMapping("{orderId}/types/{orderType}")
	public ResponseEntity<Order> findOrder(@PathVariable int orderId, @PathVariable String orderType ){
		log.info( "Finding order with orderId: {} and orderType: {}", orderId, orderType );

		Optional<Order> order = orderService.findOrder(orderId, orderType);

		return order.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}
}
